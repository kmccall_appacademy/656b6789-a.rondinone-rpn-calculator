class RPNCalculator
  # TODO: your code goes here!

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    if @stack.length < 2
      raise "calculator is empty"
    end

    sum = @stack.pop + @stack.pop
    @stack << sum
  end

  def minus
    if @stack.length < 2
      raise "calculator is empty"
    end

    num1 = @stack.pop
    num2 = @stack.pop
    diff = num2 - num1
    @stack << diff
  end

  def divide
    if @stack.length < 2
      raise "calculator is empty"
    end

    num1 = @stack.pop
    num2 = @stack.pop
    quotient = num2.fdiv(num1)
    @stack << quotient
  end

  def times
    if @stack.length < 2
      raise "calculator is empty"
    end
    product = @stack.pop * @stack.pop
    @stack << product
  end

  def tokens(string)
    tokens = string.split
    tokens.map do |char|
      if operation?(char)
        char.to_sym
      else
        char.to_i
      end
    end
  end

  def operation?(char)
    '-+*/'.include?(char)
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |char|
      case char
      when Integer
        push(char)
      else
        run_operation(char)
      end
    end
    value
  end

  def run_operation(op)
    case op
    when :+
      plus
    when :-
      minus
    when :*
      times
    when :/
      divide
    end
  end


end











#
